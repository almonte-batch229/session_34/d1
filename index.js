// [SECTION] Setup

// Default setup of ExpressJS
// require() is used to load the modules/packages we need
const express = require("express");

// Creation of an app/object
// this will act as the server of our project
const app = express();
const port = 3000;

// Setup for allowing the server to handle data from request
// also known as middlewares

app.use(express.json()); // allows the system to handle API
app.use(express.urlencoded({extended:true})); // allowing your app to read data from forms

// [SECTION] Route

// GET Method
app.get("/", (req, res) => {
	res.send("Hello World");
});

// POST Method
app.post("/hello", (req, res) => {
	res.send(`Hello There ${req.body.firstname} ${req.body.lastname}!`);
});

// Mock Database
// an array that will store user object when the "/signup" route is accessed.
let users = [];

// POST Method - SIGNUP
app.post("/signup", (req, res) => {

	console.log(req.body);

	// if the username and password is not empty, the user info will be pushed/stored in out mock database
	if(req.body.username !== "" && req.body.password !== "") {
		users.push(req.body);
		res.send(`User ${req.body.username} successlly registered!`);
	}
	else {
		res.send(`Please input BOTH username and password`);
	}
})

// PUT Method - CHANGE PASSWORD
app.put("/change-password", (req, res) => {
	// craete a variable to store the message to be sent back to the client.
	let message;

	// create a for loop that will loop through the lemesnts of "users" array
	for(let i = 0; i < users.length; i++) {

		// checks if the username provided in the client request and the username of the current object is the same
		if(req.body.username == users[i].username) {
			// changes the password of the user found by the loop
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated!`;
			break;
		}
		// if no user is found
		else {
			message = `User does not exist`;
		}
	} 

	res.send(message)
})

// [SECTION] Code Along Activity

// Homepage
app.get("/home", (req, res) => {
	res.send('Welcome to the Homepage')
})

// Registered Users
app.get("/users", (req, res) => {
	res.send(users)
})

// Delete Users
app.delete("/delete-user", (req, res) => {

	let message;

	if(users.length !== 0) {
		for(let i = 0; i < users.length; i++) {

			if(req.body.username === users[i].username) {
				users.splice(users[i], 1);
				message = `User ${req.body.username} has been deleted!`;
				break;
			}
			// if no user is found
			else {
				message = `User does not exist`;
			}
		} 
	}
	else {
		message = `No registered user`
	}

	res.send(message)
})

// usually placed at the end part of the codeblock
app.listen(port, ()=> console.log(`Server is running at port ${port}.`));